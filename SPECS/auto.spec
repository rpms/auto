Name:          auto
Version:       1.4.1
Release:       1%{?dist}
Summary:       A collection of source code generators for Java
License:       ASL 2.0
URL:           https://github.com/google/auto
Source0:       https://github.com/google/auto/archive/auto-value-%{version}.tar.gz

BuildRequires:  maven-local
BuildRequires:  mvn(com.google.guava:guava:19.0)
BuildRequires:  mvn(com.squareup:javapoet)
BuildRequires:  mvn(org.sonatype.oss:oss-parent:pom:)

BuildArch:     noarch

%description
The Auto sub-projects are a collection of code generators
that automate those types of tasks.

%package common
Summary:       Auto Common Utilities
# Obsoletes added in F30
Obsoletes:     %{name}-factory < %{version}-%{release}

%description common
Common utilities for creating annotation processors.

%package service
Summary:       Provider-configuration files for ServiceLoader

%description service
A configuration/meta-data generator for
java.util.ServiceLoader-style service
providers.

%package value
Summary:       Auto Value

%description value
Immutable value-type code generation for Java 1.6+.

%package javadoc
Summary:       Javadoc for %{name}

%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n auto-auto-value-%{version}
find -name '*.class' -print -delete
find -name '*.jar' -print -delete

# Disable factory module due to missing dep:
# com.google.googlejavaformat:google-java-format
%pom_disable_module factory build-pom.xml

%pom_xpath_set "pom:project/pom:version" 3
for p in common factory service value ;do
%pom_xpath_set "pom:project/pom:version" %{version} ${p}
%pom_xpath_remove "pom:dependency[pom:scope = 'test']" ${p}
done

%pom_remove_plugin org.apache.maven.plugins:maven-checkstyle-plugin
%pom_remove_plugin :maven-shade-plugin value
%pom_remove_plugin :maven-invoker-plugin value
%pom_remove_plugin :maven-invoker-plugin factory

%pom_xpath_set "pom:dependency[pom:artifactId = 'auto-common']/pom:version" %{version} factory service value
%pom_xpath_set "pom:dependency[pom:artifactId = 'auto-service']/pom:version" %{version} factory value
%pom_xpath_set "pom:dependency[pom:artifactId = 'auto-value']/pom:version" %{version} factory

%mvn_package :build-only __noinstall

%build
# Unavailable test deps
%mvn_build -sf -- -f build-pom.xml

%install
%mvn_install

%files -f .mfiles-%{name}-parent
%dir %{_javadir}/%{name}
%doc README.md
%license LICENSE.txt

%files common -f .mfiles-%{name}-common
%doc common/README.md
%license LICENSE.txt

%files service -f .mfiles-%{name}-service
%doc service/README.md
%license LICENSE.txt

%files value -f .mfiles-%{name}-value
%doc value/README.md
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Fri Feb 15 2019 Mat Booth <mat.booth@redhat.com> - 1.4.1-1
- Update to release 1.4.1 of auto
- Disable unused factory module due to missing deps

* Thu Jan 31 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Thu Jul 12 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Fri Jun 08 2018 Mat Booth <mat.booth@redhat.com> - 1.3-2
- Fix BRs

* Fri Jun 08 2018 Mat Booth <mat.booth@redhat.com> - 1.3-1
- Update to 1.3 release of auto

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 13 2015 gil cattaneo <puntogil@libero.it> 1.1-1
- update to 1.1

* Wed Apr 01 2015 gil cattaneo <puntogil@libero.it> 1.0-2
- enable factory module

* Tue Mar 31 2015 gil cattaneo <puntogil@libero.it> 1.0-1
- initial rpm
